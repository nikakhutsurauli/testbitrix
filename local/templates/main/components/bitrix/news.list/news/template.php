<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
<table class="table">
  <thead>
    <tr>
      <th scope="col">pubDate</th>
      <th scope="col">Name</th>
      <th scope="col">Description</th>
      <th scope="col">Category</th>
	  <th scope="col">Enclosure</th>
	  <th scope="col">Link</th>
    </tr>
  </thead>
  <tbody>
  <? if(!empty($arResult["ITEMS"])) : ?>
    <? foreach($arResult["ITEMS"] as $arItem) :?>
     <tr>
       <td><?=strtolower(FormatDate("d F Y H:i:s", MakeTimeStamp($arItem['DATE_CREATE']))) ?></td>
       <td><?=$arItem["NAME"]; ?></td>
       <td><?=$arItem["PREVIEW_TEXT"]; ?></td>
       <td><?=$arItem["PROPERTIES"]["CATEGORY"]["VALUE"];?></td>
       <td><?=$arItem["PROPERTIES"]["ENCLOSURE"]["VALUE"]["TEXT"];?></td>
       <td><?=$arItem["PROPERTIES"]["LINK"]["VALUE"];?></td>
     </tr>
    <? endforeach; ?>
  <? endif; ?>
  </tbody>
</table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>

