<?php
use Bitrix\Main\Loader;

class ImportNewsRia{

    const XML_URL = 'https://ria.ru/export/rss2/archive/index.xml';
    
    public function ImportData(){
        Loader::includeModule('iblock');
    
        $xml=simplexml_load_file(self::XML_URL);
        $el = new CIBlockElement;

        foreach($xml->channel->item as $obItem){
        
            $unixtime = strtotime($obItem->pubDate);
            $dateTime = \Bitrix\Main\Type\DateTime::createFromTimestamp($unixtime); 
                
            $PROP = array();
            $PROP[14]  = $obItem->link;   
            $PROP[15]  = $obItem->category; 
            $PROP[16] = $obItem->enclosure->attributes()->url; 
        
            $arLoadProductArray = Array(
                "IBLOCK_SECTION_ID" => false, 
                "IBLOCK_ID"         => 11,
                "PROPERTY_VALUES"   => $PROP,
                "NAME"              => $obItem->title,
                "ACTIVE"            => "Y", 
                "DATE_CREATE"       => $dateTime,
                "ACTIVE_FROM"       => $dateTime,
                "PREVIEW_TEXT"      => $obItem->description
            );
           $el->Add($arLoadProductArray);
       } 
    }
    public static function getAgent(){
        return __CLASS__.'::ImportData();';
    } 
}
   